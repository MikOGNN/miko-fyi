const fs   = require("fs");
const path = require("path");
const toml = require("toml");
const info = require("../package.json");

const outpath      = path.join(__dirname, "..", "public");
const contentsPath = path.join(__dirname, "..", "contents.toml");

let contents = {};

if (!fs.existsSync(contentsPath)) {
	console.error(`ERROR: file '${contentsPath}' not found. Did you forget to copy 'contents.toml.sample' to 'contents.toml'?`);
	process.exit(1);
}

try {
	contents = toml.parse(fs.readFileSync(contentsPath));
} catch (error) {
	console.error(`./contents.toml:${error.line}:${error.column}: ERROR: could not parse TOML: ${error.message}`);
	process.exit(1);
}

if ("entry" in contents) {
	for (const entry of contents.entry) {
		if (!entry.slug || !entry.url)
			console.error(`ERROR: entry missing slug or url: ${JSON.stringify(entry)}`);
	}
}

if (!fs.existsSync(outpath)) {
	console.error(`ERROR: could not write html files: '${outpath}' does not exist.`);
	process.exit(1);
}

const entireFile = filepath => {
	if (!fs.existsSync(filepath)) {
		console.error(`ERROR: could not read file '${filepath}'`);
		process.exit(1);
	}
	return fs.readFileSync(filepath, "utf-8");
};

const writeFile = (filename, contents) => {
	const filepath = path.join(outpath, filename);

	fs.writeFileSync(filepath, contents);
};

const fromTemplate = (source, context = {}, literal = false) => {
	let template = !literal ? entireFile(path.join(__dirname, "..", "templates", source)) : source;

	for (const [key, value] of Object.entries(context)) {
		const regex = new RegExp(`{{${key}}}`, "mg");
		template = template.replace(regex, value);
	}

	return template;
};

const hasImprint = "imprint" in contents;
const hasPrivacy = "privacy" in contents; 

const footer = fromTemplate("footer.html", {
	imprint    : hasImprint ? "<a href='imprint.html' data-i18n>Imprint</a>"        : "",
	privacy    : hasPrivacy ? "<a href='privacy.html' data-i18n>Privacy Policy</a>" : "",
	gitlabLink : info.repository.url ?? "https://gitlab.com",
	version    : info.version
});

const context = {
	sitename       : contents.sitename ? `${contents.sitename} – ` : "",
	title          : contents.sitename ?? "",
	gitlabLink     : info.repository.url ?? "https://gitlab.com",
	items_per_page : contents.items_per_page ?? 20,
	footer
};

writeFile("index.html", fromTemplate("index.html", context));
writeFile("404.html", fromTemplate("404.html", context));
writeFile("urls.json", "entry" in contents ? JSON.stringify(contents.entry) : "[]");
writeFile(path.join("javascript", "LinkList.js"), fromTemplate(
	entireFile(path.join(__dirname, "..", "javascript", "LinkList.js")), context, true)
);

if (hasImprint) {
	writeFile("imprint.html", fromTemplate("page.html", {
		...context,
		name    : "Imprint",
		content : contents.imprint.trim(),
	}));
}

if (hasPrivacy) {
	writeFile("privacy.html", fromTemplate("page.html", {
		...context,
		name    : "Privacy Policy",
		content : contents.privacy.trim(),
	}));
}