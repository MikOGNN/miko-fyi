import $ from "./Selector.js";

class I18n {
	translations;
	language;

	constructor() {
		this.translations = {};
		this.language     = "language" in navigator ? navigator.language.substring(0, 2) : "en";

		fetch("i18n.json").then(response => response.json()).then(json => {
			this.translations = json;
			this.translate();
		}).catch(error => console.error(`ERROR: could not load translations: ${error}`));
	}

	translate() {
		const language = this.translations[this.language] ? this.language : null;
		if (!language)
			return;

		const elements = $("[data-i18n]");
		for (const element of elements) {
			const special = element.getAttribute("data-i18n");
			const target  = special === "" ? "textContent" : special

			if (!this.translations[language][element[target].trim()])
				continue;
			element[target] = this.translations[language][element[target].trim()];
		}
	}
}

export default I18n;