import $ from "./Selector.js";

const PER_PAGE = {{items_per_page}};

class LinkList {
	urls;
	filteredUrls;
	field;
	list;
	page;

	constructor() {
		this.urls         = [];
		this.filteredUrls = [];
		this.field        = $("#filter");
		this.list         = $("#articleList");
		this.page         = 0;

		if (!(this.field instanceof HTMLElement))
			return;

		fetch("urls.json").then(response => response.json()).then(json => {
			this.update(json);
		}).catch(error => console.error(`ERROR: could not load URLs: ${error}`));

		this.field.addEventListener("input", event => this.filter(event));
	}

	get pages() {
		return Math.ceil(this.filteredUrls.length / PER_PAGE);
	}

	update(newUrls) {
		this.urls         = [...newUrls];
		this.filteredUrls = [...newUrls];

		this.render();
	}

	filter(event) {
		const filterValue = event.target.value;

		if (filterValue.length < 3)
			this.filteredUrls = [...this.urls];
		else {
			const regex = new RegExp(`.*${LinkList.EscapeRegexp(filterValue)}.*`, "gi");
			this.filteredUrls = this.urls.filter(item => item.slug?.match(regex) || item.description?.match(regex));
		}
		this.render();
	}
	
	render() {
		const hasEntries = this.filteredUrls.length > 0;
		const template   = hasEntries ? $("#article").content.firstElementChild : $("#nope").content.firstElementChild;

		this.list.innerHTML = "";

		if (!hasEntries) {
			this.list.append(template.cloneNode(true));
			return;
		}

		const start = this.page * PER_PAGE;
		const items = this.filteredUrls.slice(start, start + PER_PAGE);
	
		for (const url of items) {
			const article = template.cloneNode(true);

			article.querySelector(".slug").textContent = url.slug;
			article.querySelector(".link").href        = url.url;
			article.querySelector("main").textContent  = url.description;
			this.list.append(article);
		}

		this.renderPagination();
	}

	renderPagination() {
		if (this.pages < 2)
			return;

		const template = document.createElement("template");

		template.innerHTML = `
			<nav class=pagination>
			</nav>
		`;
		const pagination = template.content.firstElementChild;

		for (let i = 1; i <= this.pages; ++i) {
			console.log("link");
			const link = document.createElement("a");
			
			link.href        = "#"
			link.textContent = i;
			link.addEventListener("click", event => {
				event.preventDefault();
				this.page = i - 1;
				this.render();
			});
			if (i - 1 === this.page)
				link.classList.add("active");
			pagination.append(link);
		}

		this.list.append(pagination);
	}

	static EscapeRegexp(string) {
		return string.replace(/[|\\{}()[\]^$+*?.]/g, '\\$&').replace(/-/g, '\\x2d');
	}
}

export default LinkList;
